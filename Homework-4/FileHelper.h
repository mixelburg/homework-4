#pragma once
#include <string>


class FileHelper
{
	public:
		static std::string readFileToString(const std::string& fileName);
		static void write_words_to_file(const std::string& inputFileName, const std::string& outputFileName);
};
