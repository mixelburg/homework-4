#pragma once
#include "ShiftText.h"

class CaesarText : public ShiftText
{
	public:
		CaesarText(std::string& str);
		~CaesarText();
		friend std::ostream& operator<<(std::ostream& os, const CaesarText& txt);
};



