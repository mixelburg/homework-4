#include "CaesarText.h"

CaesarText::CaesarText(std::string& str) : ShiftText(str, 3) {};

CaesarText::~CaesarText() {};

std::ostream& operator<<(std::ostream& os, const CaesarText& txt)
{
	os << "Caesar Text" << std::endl << txt.text;
	return os;
}
