#include "SubstitutionText.h"
#include <fstream>
#include <vector>
#include <string>
#include <sstream>

SubstitutionText::SubstitutionText(std::string& txt, std::string& dictName) : PlainText(txt), _dictionaryFileName(dictName) {};

SubstitutionText::~SubstitutionText() {};
	
/**
 * @brief encrypts this.text field
 * @return encrypted text
*/
std::string SubstitutionText::encrypt()
{
	// open dictionary file for reading
	std::fstream fin;
	fin.open(this->_dictionaryFileName, std::ios::in);
	
	// create vector to store the dictionary
	std::vector<std::pair<char, char>> rows;
	std::string word, temp;

	// push all values from dictionary to the vector
	for (std::string line; std::getline(fin, line);)
	{
		// parse the one line and push values from is
		rows.push_back({ line.c_str()[0], line.c_str()[2] });
	}

	// encrypt this.text according to the dictionary
	for (int i = 0; i < this->text.length(); i++) // loop through each letter in text string
	{
		for (std::pair<char, char> row : rows) // loop though each pair of chars in dictionary vector
		{
			if (this->text[i] == row.first) // if needed pair is found - swap char from string to char from dictionary 
			{
				this->text[i] = row.second;
				break; // break from loop after finding the correct pair
			}
		}
	}
	// close the file
	fin.close();
	
	this->isEncrypted = true; // change isEncrypted indicator to true
	return this->text;
}

/**
 * @brief decrypts this.text field
 * @return decrypted field
*/
std::string SubstitutionText::decrypt()
{
	// open dictionary file for reading
	std::fstream fin;
	fin.open(this->_dictionaryFileName, std::ios::in);

	// create vector to store the dictionary
	std::vector<std::pair<char, char>> rows;
	std::string word, temp;
	
	// push all values from dictionary to the vector
	for (std::string line; std::getline(fin, line);)
	{
		// parse the one line and push values from is
		rows.push_back({ line.c_str()[0], line.c_str()[2] });
	}

	// dencrypt this.text according to the dictionary
	for (int i = 0; i < this->text.length(); i++) // loop through each letter in text string
	{
		for (std::pair<char, char> row : rows) // loop though each pair of chars in dictionary vector
		{
			if (this->text[i] == row.second)  // if needed pair is found - swap char from string to char from dictionary 
			{
				this->text[i] = row.first;
				break; // break from loop after finding the correct pair
			}
		}
	}
	// close the file
	fin.close();
	
	this->isEncrypted = false; // change isEncrypted indicator to true
	return this->text;
}

std::ostream& operator<<(std::ostream& os, const SubstitutionText& txt)
{
	os << "Substitution Text" << std::endl << txt.text;
	return os;
}
