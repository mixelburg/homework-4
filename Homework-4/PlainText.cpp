#include "PlainText.h"
#include <iostream>

int PlainText::_NumOfTexts = 0;

PlainText::PlainText(std::string& str) : text(str), isEncrypted(false)
{
	PlainText::_NumOfTexts++;
};
PlainText::~PlainText() {};

// simple getters
bool PlainText::isEnc() const
{
	return this->isEncrypted;
}

std::string PlainText::getText() const
{
	return this->text;
}

int PlainText::get_num_texts()
{
	return PlainText::_NumOfTexts;
}

std::ostream& operator<<(std::ostream& os, const PlainText& txt)
{
	os << "Plain Text" << std::endl << txt.text;
	return os;
}
