// Homework-4.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <map>

#include "ShiftText.h"
#include "CaesarText.h"
#include "SubstitutionText.h"
#include "FileHelper.h"
#include "BonusText.h"

// enum for choice numbers
enum choices
{
	// text processing
	process_plain_text_c = 1,
	process_shift_text_c = 2,
	process_caesar_text_c = 3,
	process_substitution_text_c = 4,

	// displaying data
	display_file_data_c = 1,
	save_file_data_c = 2,

	// encrypt - decrypt
	encrypt_c = 1,
	decrypt_c = 2,

	// menus
	string_menu_c = 1,
	file_menu_c = 2,
	display_num_texts_c = 3,
	exit_c = 4,
};

// text id's
enum msgs
{
	// info messages
	enter_choice = 101,
	enter_file_name = 102,
	enter_key = 103,
	enter_string = 104,
	enter_dict_path = 105,

	// exit message
	exit_msg = 201,

	// status messages
	num_of_texts = 301,

	// error messages
	invalid_choice_error = 401,
	input_type_error = 402,
};
// map with texts
std::map<msgs, std::string> MSG = {
	// info messages
	{enter_choice, "enter choice: "},
	{enter_file_name, "Enter file name: "},
	{enter_key, "enter key: "},
	{enter_string, "Enter string you want to encrypt: "},
	{enter_dict_path, "enter dict file name (file path): "},

	// exit message
	{exit_msg, "[+] exiting..."},

	// status messages
	{num_of_texts, "Num of texts: "},

	// error messages
	{invalid_choice_error, "[!] invalid choice"},
	{input_type_error, "[!] only numbers allowed, try again"},
};


//// menu printers

// main menu
void print_menu()
{
	std::cout << std::endl
		<< "Main menu: " << std::endl
		<< "1. String encryption" << std::endl
		<< "2. File encryption" << std::endl
		<< "3. Print number of texts" << std::endl
		<< "4. Exit" << std::endl
		<< std::endl;
}
// string menu
void print_string_menu()
{
	std::cout << std::endl
		<< "1. Plain text" << std::endl
		<< "2. Shift" << std::endl
		<< "3. Caesar" << std::endl
		<< "4. Substitution" << std::endl
		<< std::endl;
}
// file processing menu
void print_file_encrypt_decrypt_menu()
{
	std::cout << std::endl
		<< "1. Encrypt file" << std::endl
		<< "2. Decrypt file" << std::endl
		<< std::endl;
}
// file data menu
void print_file_data_menu()
{
	std::cout << std::endl
		<< "1. Display result" << std::endl
		<< "2. Save result" << std::endl
		<< std::endl;
}

// helper functions

/**
 * @brief checks if string is integer number
 * @param s string
 * @return bool
*/
bool is_number(const std::string& s)
{
	// get iterator
	std::string::const_iterator it = s.begin();
	// go trough string
	while (it != s.end() && std::isdigit(*it)) ++it;

	// check if there is any non digit char
	return !s.empty() && it == s.end();
}

/**
 * @brief converts string to int
 * @param str string
 * @return int from string
*/
int to_int(const std::string& str)
{
	// get string stream
	std::stringstream sstream(str);
	// create variable for the result
	int result;
	// convert string to int
	sstream >> result;
	
	return result;
}

/**
 * @brief gets integer number from user 
 * @param message - message to be printed to user
 * @return integer number from user
*/
int get_num(std::string& message)
{
	// variable for usr input
	std::string input;

	// display given message
	std::cout << message;

	// get input from user as a string
	std::getline(std::cin, input);

	// get new input until it isn't correct
	while (!(is_number(input)))
	{
		// print error message
		std::cout << MSG[input_type_error] << std::endl;
		// print input message
		std::cout << message;
		// get new input
		std::cin >> input;
	}
	return to_int(input);
}

/**
 * @brief gets string from user
 * @param message - message to be printed to user
 * @return string input from user
*/
std::string get_string(std::string& message)
{
	// create variable for input
	std::string str;
	// display given message
	std::cout << message;
	// get string from user
	std::getline(std::cin, str);
	
	return str;
}

/**
 * @brief processes plain text
 * @param text given text string
 * @return processed text string
*/
std::string process_plain_text(std::string& text)
{
	// create PlainText object and initialize it with given string
	const PlainText txt(text);
	return txt.getText();
}

/**
 * @brief processes shift text
 * @param text given text string
 * @param choice decrypt or encrypt choice 
 * @return processed text string
*/
std::string process_shift_text(std::string& text, const int choice = choices::encrypt_c)
{
	// get encryption key from user
	const int key = get_num(MSG[enter_key]);
	
	// create ShiftText object and initialize it with given string and key  
	ShiftText shift_txt(text, key);

	// encrypt or decrypt text based on given parameter "choice"
	if (choice == choices::encrypt_c) shift_txt.encrypt();
	else if (choice == choices::decrypt_c) shift_txt.decrypt();
	
	return shift_txt.getText();
}

/**
 * @brief processes caesar text
 * @param text given text string
 * @param choice decrypt or encrypt choice
 * @return processed text string
*/
std::string process_caesar_text(std::string& text, const int choice = choices::encrypt_c)
{
	// create CaesarText object and initialize it with text from user
	CaesarText caesar_txt(text);

	// encrypt or decrypt text based on given parameter "choice"
	if (choice == choices::encrypt_c) caesar_txt.encrypt();
	else if (choice == choices::decrypt_c) caesar_txt.decrypt();

	return caesar_txt.getText();
}

/**
 * @brief processes substitution text
 * @param text given text string
 * @param choice decrypt or encrypt choice
 * @return processed text string
*/
std::string process_substitution_text(std::string& text, const int choice = choices::encrypt_c)
{
	// get encryption dictionary path from user
	std::string dict_file_name = get_string(MSG[enter_dict_path]);

	// create SubstitutionText object and initialize it with given text string and dictionary path
	SubstitutionText substitution_txt(text, dict_file_name);

	// encrypt or decrypt text
	if (choice == choices::encrypt_c) substitution_txt.encrypt();
	else if (choice == choices::decrypt_c) substitution_txt.decrypt();

	return substitution_txt.getText();
}

/**
 * @brief processes string menu
*/
void string_menu()
{
	// get string to process from user
	std::string str = get_string(MSG[enter_string]);

	// print menu
	print_string_menu();
	// get user choice
	const int choice = get_num(MSG[enter_choice]);

	// process user choice
	switch (choice)
	{
		case choices::process_plain_text_c:
			std::cout << process_plain_text(str) << std::endl;
			break;
		case choices::process_shift_text_c:
			std::cout << process_shift_text(str) << std::endl;
			break;
		case choices::process_caesar_text_c:
			std::cout << process_caesar_text(str) << std::endl;
			break;
		case choices::process_substitution_text_c:
			std::cout << process_substitution_text(str) << std::endl;
			break;
		default:
			std::cout << MSG[invalid_choice_error] << std::endl;
			break;
	}
}

/**
 * @brief saves data from given string to file
 * @param data given string with some data
*/
void save_data_to_file(std::string& data)
{
	// get file name from user
	const std::string file_name = get_string(MSG[enter_file_name]);

	// open file for writing
	std::ofstream out_file;
	out_file.open(file_name);

	// write data to file
	out_file << data;

	// close the file
	out_file.close();
}

/**
 * @brief processes file menu
*/
void file_menu()
{
	// get file name from user
	const std::string file_name = get_string(MSG[enter_file_name]);
	// save data from file to string
	std::string file_data = FileHelper::readFileToString(file_name);

	// print manu
	print_string_menu();
	// get choice from user
	int choice = get_num(MSG[enter_choice]);
	
	int to_do;
	// go to encryption menu if text isn't plain
	if(choice != choices::process_plain_text_c)
	{
		// print encryption menu
		print_file_encrypt_decrypt_menu();
		// get choice from user
		to_do = get_num(MSG[enter_choice]);

		// check if choce is valid
		if (!(to_do == choices::encrypt_c || to_do == choices::decrypt_c))
		{
			std::cout << MSG[invalid_choice_error] << std::endl;
			return;
		}
	}

	// process data from file according to user choice
	switch (choice)
	{
		case choices::process_plain_text_c:
			file_data = process_plain_text(file_data);
			break;
		case choices::process_shift_text_c:
			file_data = process_shift_text(file_data, to_do);
			break;
		case choices::process_caesar_text_c:
			file_data = process_caesar_text(file_data, to_do);
			break;
		case choices::process_substitution_text_c:
			file_data = process_substitution_text(file_data, to_do);
			break;
		default:
			std::cout << MSG[invalid_choice_error] << std::endl;
			return;
	}

	// print data representation menu
	print_file_data_menu();
	// get user choice
	choice = get_num(MSG[enter_choice]);

	// save data or display it according to user choice
	switch (choice)
	{
	case choices::display_file_data_c:
		std::cout << file_data << std::endl;
		break;
	case choices::save_file_data_c:
		save_data_to_file(file_data);
		break;
	default:
		std::cout << MSG[invalid_choice_error] << std::endl;
		return;
	}
}

/**
 * @brief main menu processor function
 * @param choice user choice
*/
void main_menu(const int choice)
{
	// go to submenu according to user choice
	switch (choice)
	{
		case choices::string_menu_c:
			string_menu();
			break;
		case choices::file_menu_c:
			file_menu();
			break;
		case choices::display_num_texts_c:
			std::cout << MSG[num_of_texts] << PlainText::get_num_texts() << std::endl;
			break;
		case choices::exit_c:
			std::cout << MSG[exit_msg] << std::endl;
			break;
		
		default:
			std::cout << MSG[invalid_choice_error] << std::endl;
			print_menu();
			break;	
	}
}

int main()
{
	int choice;
	// get new user choice until it isn't "exit choice" 
	do
	{
		print_menu();
		choice = get_num(MSG[enter_choice]);
		main_menu(choice);
	}
	while (choice != choices::exit_c);


	// BONUS
	/*std::string b_text = "Hello World!";
	BonusText bonus_text(b_text);
	bonus_text.decrypt("C:/Users/mixel/source/repos/Homework-4/Homework-4/decrypter");
	std::cout << bonus_text.getText() << std::endl;
	std::cout << bonus_text;*/
	
}


