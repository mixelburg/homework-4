#pragma once
#include "PlainText.h"
#include <iostream>


class ShiftText : public PlainText
{
	private:
		int _key;
	public:
		ShiftText(std::string& str, int num);
		~ShiftText();

		/**
		 * @brief encrypts this.text according to this.key
		 * @return encrypted text
		*/
		std::string encrypt();

		/**
		 * @brief decrypts this.text according to this.key
		 * @return decrypted text
		*/
		std::string decrypt();

		friend std::ostream& operator<<(std::ostream& os, const ShiftText& txt);
};

